/*
 *	Audio player for the phytotron
 */
 
#include  <stdio.h>
#include  <stdlib.h>
#include <unistd.h>
#include  "player.h"

/* --------------------------------------------------------------------- */

int		verbosity;

/* --------------------------------------------------------------------- */
void help(int v)
{
puts("Options :");
puts("\t-w filename.wav");
exit(0);
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
char		*wavname = "1337.wav";
int		foo, opt, K;


printf("\n**** %s **** compiled the %s at %s ***\n",
		argv[0], __DATE__, __TIME__);


while ((opt = getopt(argc, argv, "hK:vw:")) != -1) {
	switch (opt) {
		case 'h':	help(0);		break;
		case 'K':	K = atoi(optarg);	break;
		case 'v':	verbosity++;		break;
		case 'w':	wavname = optarg;	break;
		default:				break;
		}
	}



foo = blast_this_file(wavname);
fprintf(stderr, "audio blaster -> %d\n", foo);

return 0;
}
/* --------------------------------------------------------------------- */
