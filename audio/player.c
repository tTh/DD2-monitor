/*
 *	Audio player for the phytotron
 */
 
#include  <stdio.h>
#include  <unistd.h>
#include  <string.h>
#include  <fcntl.h>

#include <ao/ao.h>
#include <sndfile.h>

/* --------------------------------------------------------------------- */

#define   T_BUFF_WAVES          (8192*4)
#define   T_FNAME               1000
static ao_device                *device;
static ao_sample_format         format;
static int                      coefficient;
static int16_t          	buffer[T_BUFF_WAVES];

/* --------------------------------------------------------------------- */
int init_waveout(int smplrate)
{
int                     default_driver;

ao_initialize();

default_driver = ao_default_driver_id();
#if DEBUG_LEVEL
fprintf(stderr, "%s : ao default driver #%d\n", __func__, default_driver);
#endif

memset(&format, 0, sizeof(format));
format.bits = 16;
format.channels = 2;
format.rate = smplrate;
format.byte_format = AO_FMT_LITTLE;

device = ao_open_live(default_driver, &format, NULL);
if (device == NULL) {
        fprintf(stderr, "Error opening AO device.\n");
        return -1;
        }

return 0;
}

/* --------------------------------------------------------------------- */
int close_waveout(void)
{
ao_close(device);
ao_shutdown();
return 0;
}
/* --------------------------------------------------------------------- */

int blast_this_file(char *fname)
{
SNDFILE                 *sndf;
SF_INFO                 sfinfo;
int			foo, lus;

sndf = sf_open(fname, SFM_READ, &sfinfo);
if (NULL==sndf)
        {
        perror("sf_open");
        return -2;
        }

#if DEBUG_LEVEL
fprintf(stderr, "samplerate : %d\n", sfinfo.samplerate);
fprintf(stderr, "frames     : %ld\n", sfinfo.frames);
fprintf(stderr, "seekable   : %d\n", sfinfo.seekable);
#endif

foo = init_waveout(sfinfo.samplerate);
if (foo) {
	fprintf(stderr, "init waveout -> %d\n", foo);
	}

while ((lus = sf_read_short(sndf, buffer, T_BUFF_WAVES)))
        {
#if DEBUG_LEVEL      
	fprintf(stderr, "%s : %d bytes read\n", fname, lus);
#endif
        /* send all the bits to the audio output  */
        foo = ao_play(device, (char *)buffer, lus*2);
	if (foo) {
		fprintf(stderr, "ao_play -> %d\n", foo);
		}
	}
	
/* do some cleanup */
close_waveout();
sf_close(sndf);

return 0;
}

/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
