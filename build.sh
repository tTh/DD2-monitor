#!/bin/bash


# ------------------------------------------------------------------

function build
{
echo ============= $1 ==============
curdir=${PWD}
cd $1
make t
error=$?
cd ${curdir}

if [ ${error} -ne 0 ]
then
	echo === error on $1 = ${error}
	exit
fi
}

# ------------------------------------------------------------------
#
#	euh, il faut parfois tweaker l'ordre de ces modules
#
modules=" core serial viz/curses ui audio "

for mod in ${modules}
do
	build $mod
done

echo +++++++++++++++++++++++++++++++++++

make essai


# ------------------------------------------------------------------
