/*
 *		fake-values.c
 *		-------------
 *		useless software
 */

#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>

#include   "core/utils.h"

int verbosity;

/* --------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		opt;
int		type = 0;

while ((opt = getopt(argc, argv, "vst:")) != -1) {
	switch (opt) {
		case 'v':	verbosity++;		break;
		case 's':	srand(getpid());	break;
		case 't':	type = atoi(optarg);	break;	
		default:				break;
		}
	}

if (verbosity > 1) {
	fprintf(stderr, "fake values - %s %s\n", __DATE__, __TIME__);
	}

printf("%.3f %4d\n", dtime(), random1000(type));

return 0;
}

/* --------------------------------------------------------------- */
