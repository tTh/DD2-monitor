#########################################################

#		MAIN PROGGY

#	must be run with gnu make
#

CC	= gcc
CCOPT	= -Wall -g 
CLIB	= core/libdd2m-core.a viz/libdd2m-viz.a audio/player.o

all:	essai 

# -------------------------------------------------------

OSERIAL	= serial/serial.o serial/funcs.o ui/terminal.o

essai.o:	essai.c  Makefile 
	$(CC) ${CCOPT} $< -c 

essai:	essai.o Makefile $(CLIB) ${OSERIAL}
	$(CC) ${CCOPT} $< ${OSERIAL} $(CLIB)	\
		-lncurses -lao -lsndfile -o $@

fake-values:	fake-values.c  Makefile $(CLIB)
	$(CC) ${CCOPT} $< $(CLIB) -o $@

# -------------------------------------------------------

