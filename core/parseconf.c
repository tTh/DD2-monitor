/*
 *		core/parseconf.c
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#include  "config.h"

extern int 			verbosity;
extern Configuration		config;

#define  CMP(a)   (!strcmp(keyptr, a))

/* ---------------------------------------------------------------- */
int parse_config(char *fname, int flags)
{
FILE		*fp;
char		line[SZ_STRINGS+1],
		*keyptr, *typeptr, *cptr;
int		numligne;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' $%x )\n", fname, flags);
#endif

config.valid = 49;

if (NULL==(fp=fopen(fname, "r"))) {
	perror(fname);
	return -2;
	}

numligne = 0;

while (fgets(line, SZ_STRINGS, fp))
	{
	numligne++;
	if ('\0'==line[0]) {
		fprintf(stderr, "%s : short read line %d\n", 
				fname, numligne);
		fclose(fp);
		return -1;
		}

	/* massage the end of line */
	line[strlen(line)-1] = '\0';          /* kill EOL */
	if (verbosity) {
		fprintf(stderr, "%3d :\t%s\n", numligne, line);
		}

	/* seek for the first token in this line */
	if (NULL==(keyptr = strtok(line, " \t"))) {
		/* Got an empty line */
		continue;
		}
	/* skip comments */
	if ('#'==*keyptr)		continue;
	/* seek for the type field */
	if (NULL==(typeptr = strtok(NULL, " \t"))) {
		/* we can(t get a type flag ? wtf ? */
		fprintf(stderr, "ERROR line %d : no type\n", numligne); 
		continue;
		}

	if(verbosity)
		fprintf(stderr, "[%s] type %s\n", keyptr, typeptr);

	if CMP("abort") { 
		fprintf(stderr, "abort in config file\n");
		}

	if (CMP("input_device")) {
		config.input_device = strdup(strtok(NULL, " \t"));
		continue;
		}

	if (CMP("input_speed")) {
		config.input_speed = atoi(strtok(NULL, " \t"));
#if DEBUG_LEVEL
		fprintf(stderr, "input speed = %d\n", config.input_speed);
#endif
		}

	if (CMP("eyecandy_banner")) {
		config.eyecandy_banner = strdup(strtok(NULL, " \t"));
		continue;
		}

	}

fclose(fp);

return 0;
}
/* ---------------------------------------------------------------- */
int show_config(char *title)
{

if (verbosity) {
	printf("********** %s **********\n", title);
	}

printf("valid           :  %d\n", config.valid);
printf("input device    :  %s\n", config.input_device);
printf("input speed     :  %d\n", config.input_speed);

puts("");
return 0;
}
/* ---------------------------------------------------------------- */
