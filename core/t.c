/*
 *	main de test des core functions
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <getopt.h>

#include	"lut1024.h"
#include	"config.h"
#include  	"sysmetrics.h"

int verbosity;

Configuration		config;


/* ---------------------------------------------------------------- */
int essai_sysmetrics(int k)
{
float		fvalues3[3];
int		foo;

foo = get_loadavg(fvalues3);
if (foo) {
	fprintf(stderr, "err get load avg %d\n", foo);
	return -1;
	}

printf("load avg %f %f %f\n", fvalues3[0], fvalues3[1], fvalues3[2]);

return 0;
}
/* ---------------------------------------------------------------- */

int main (int argc, char *argv[])
{
int		foo, opt;
char		*conffile = "dd2-monitor.conf";

fprintf(stderr, "+\n+   DD2 MONITOR\n+\n");

/* set some default values */
verbosity	= 0;


while ((opt = getopt(argc, argv, "v")) != -1) {
	switch (opt) {
		case 'v':	verbosity++;		break;

		default:
			fprintf(stderr, "%s : uh ?", argv[0]);
			exit(1);
			break;
		}

	}


foo = parse_config(conffile, 0);
fprintf(stderr, "parse_config(%s) -> %d\n\n", conffile, foo);
show_config("foo");

essai_sysmetrics(0);
/*
foo = load_lut1024f("foo.lut1024f", NULL, 1);
fprintf(stderr, "chargement de la lut --> %d\n\n", foo);
*/

return 0;
}

/* ---------------------------------------------------------------- */
