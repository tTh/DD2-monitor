/*
 *		core/sysmetrics.c
 */

#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>
#include    <string.h>
#include  <time.h>

#include  "sysmetrics.h"

extern int	verbosity;

/* --------------------------------------------------------------- */

int get_loadavg(float *where)
{
FILE		*fp;
float		loads[3];
int		foo;

if ( ! (fp=fopen("/proc/loadavg", "r")) ) {
	perror("read loadavg");
	return -1;
	}

foo = fscanf(fp, "%f %f %f", loads, loads+1, loads+2);
if (3 != foo) fprintf(stderr, "%s : read %d vals\n", __func__, foo);

memcpy(where, loads, 3 * sizeof(float));

fclose(fp);

return 0;	
}

/* --------------------------------------------------------------- */
