/*
 *		config.h
 */

#define		SZ_STRINGS	200

typedef struct {

	int	valid;
	
	char	*input_device;
	int	input_speed;

	char	*eyecandy_banner;

	} Configuration;

/* ---------------------------------------------------------------- */

int set_default_config(Configuration *cfg);
int parse_config(char *fname, int flags);
int show_config(char *title);

/* ---------------------------------------------------------------- */
