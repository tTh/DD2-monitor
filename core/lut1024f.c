/*
 *	LUT 1024 -> FLOAT
 */

#include	<stdio.h>
#include	<stdlib.h>
#include 	<string.h>

#include	"lut1024.h"

extern int	verbosity;

/* ---------------------------------------------------------------- */

int slurp_lut1024f(FILE *fp, Lut1024f *where, int notused)
{
int	count, foo;

for(count=0; count<1024; count++) {
	foo = fscanf(fp, "%f", &where->vals[foo]);
	if (1 != foo) {
		fprintf(stderr, "%s: bad read %d\n", __func__, foo);
		return -4;
		}
	}

return 0;
}

/* ---------------------------------------------------------------- */

int load_lut1024f(char *fname, Lut1024f *where, int notused)
{
FILE	*fplut;
char	firstline[100];
char	label[] = "LUT1024F";
int	foo;

#if DEBUG_LEVEL
fprintg(stderr, ">>> %s ( '%s' %p %d )\n", __func__,
			fname, where, notused);
#endif

if (NULL==(fplut=fopen(fname, "r"))) {
	perror(fname);
	return -2;
	}

fprintf(stderr, "%s: getting first line\n", __func__);

if (NULL==fgets(firstline, 20, fplut)) {
	fprintf(stderr, "%s: nothing to read from %s\n",
					__func__, fname);
	return -3;
	}

foo = strncmp(label, firstline, sizeof(label)-1);
if (foo) {
	fprintf(stderr, "%s: bad label [%s]\n", __func__, firstline);
	exit(5);
	}

foo = slurp_lut1024f(fplut, where, 0);

fclose(fplut);

return -1;
}

/* ---------------------------------------------------------------- */
