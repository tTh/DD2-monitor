/*
 *	LUT 1024 - DEALING WITH DISCRETE VALUES 
 */


typedef struct {
	int		flags;
	float		vals[1024];
	} Lut1024f;

/* ---------------------------------------------------------------- */

int slurp_lut1024f(FILE *fp, Lut1024f *where, int notused);
int load_lut1024f(char *fname, Lut1024f *where, int notused);

/* ---------------------------------------------------------------- */

