/*
 *	core/utils.c
 */

#include  <stdio.h>
#include <unistd.h>
#include  <stdlib.h>
#include  <string.h>
#include  <ctype.h>
#include  <time.h>
#include  <sys/time.h>

#include  "utils.h"

extern int verbosity;

/* --------------------------------------------------------------- */
int special_dumper(FILE *fp, unsigned char octet)
{
static int		idx = 0;
static unsigned char	buffer[16];

int			foo, c;

#if DEBUG_LEVEL > 1
fprintf(stderr, "%s $%x %c\n", __func__, octet, octet);
#endif

buffer[idx++] = octet;

if (idx==16) {
	fprintf(fp, "dmp : ");
	for (foo=0; foo<16; foo++) {
		fprintf(fp, "%02x ", buffer[foo]);
		if (7==foo)	fprintf(fp, " ");
		}
	fprintf(fp, "    |");
	for (foo=0; foo<16; foo++) {
		c = buffer[foo];
		fprintf(fp, "%c", isprint(c) ? c : ' ');
		}
	fprintf(fp, "|\n");	fflush(fp);
	idx = 0;
	}

return 0;
}
/* ---------------------------------------------------------------- */
/* maybe not a really good initialisation... */
int seed_my_rand(int foo)
{
long	v1, v2;

v1 = getpid();		v2 = time(NULL);
return v1 ^ v2;
}
/* --------------------------------------------------------------- */
int random1000(int type)
{
int         value, foo;

#if DEBUG_LEVEL > 1
fprintf(stderr, ">>> %s(%d)\n", __func__, type);
#endif

switch (type) {
	case 0:
		value = rand() % 1000;
		break;
	case 1:
		value = (rand()%1000 + rand()%1000) / 2;
		break;
	case 4:
		value = 0;
		for (foo=0; foo<4; foo++)
			value += rand() % 1000;
		value /= 4;
		break;
	default:
		value = -1;
		break;
	}

return value;
}
/* --------------------------------------------------------------- */
double dtime(void)
{
struct timeval tv;
int         foo;

foo = gettimeofday(&tv, NULL);
if (foo) fprintf(stderr, "got %d in %s\n", foo, __func__);

return (double)tv.tv_sec + (double)tv.tv_usec / 1e6;
}
/* --------------------------------------------------------------- */
