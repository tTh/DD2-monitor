/*
 *		essais de gadgets UI
 */

#include  <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include  <ncurses.h>

#include  "../serial/serial.h"
#include  "terminal.h"

int	verbosity;

/* ---------------------------------------------------------------- */

void help(int k)
{
puts("options : ");
puts("\t-d\tserial device to read.");
puts("\t-K\tset the K parameter.");
puts("\t-v\tincrease verbosity.");

exit(0);
}
/* ---------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		opt, foo;
int		serial_in;
char		*device = "/dev/ttyS0";
int		K = 0;
// char		ligne[100];

while ((opt = getopt(argc, argv, "d:hKv")) != -1) {
	switch (opt) {
		case 'd':	device = optarg;	break;
		case 'h':	help(0);		break;
		case 'K':	K = atoi(optarg);	break;
		case 'v':	verbosity++;		break;
		default:				break;
		}
	}

printf("\n*** compiled %s %s\n", __DATE__, __TIME__);
printf("*** device: %s\n", device);

serial_in = prepare_UART(device, 9600);
if (serial_in < 0) {
	fprintf(stderr, "err open %s : %d\n", device, serial_in);
	exit(1);
	}

sleep(1);

foo = essai_terminal(serial_in, device, K);
fprintf(stderr, "essai terminal -> %d\n", foo);
return 0;
}
/* ---------------------------------------------------------------- */

