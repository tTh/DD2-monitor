#!/bin/bash

#           THIS IS A KLUDGE 

nbsamp=3000	# nombre d'echantillon 

if [ $# -eq 1 ]; then
	nbsamp=$1
fi

DATAFILE="serial/foo.dat"
IMAGE="graphe.png"
TMPFILE="/dev/shm/tmpdata"

echo $nbsamp
wc -l ${DATAFILE}

tail -${nbsamp} < ${DATAFILE} > ${TMPFILE}

gnuplot << __EOC__
set term png 	size 1000,600
set output 	"${IMAGE}"
set ytics	2
set xtics	
set grid	front
set title	"* Temperatures du Phytotron *"
set xdata 	time
set timefmt 	"%s"
set format x 	"%b %d\n%H:%M"
set yrange [ 10.0 : 40.0 ]
plot	"${TMPFILE}" using 1:3 title " inside" with lines, \
	"${TMPFILE}" using 1:4 title "ambient" with lines
__EOC__

display ${IMAGE} &


