## DD2 monitoring tools

Expérimentations et recherches dans le but de construire un système
de _monitoring_ pour le futur Phytotron du Tetalab.

Le but premier de ce système est de faciliter la mise au point d'un
automate de contrôle de l'enceinte thermostatée. Nous devrons
surveiller température et humidité du dd2, et température du
confinement biologique. 


Pour en savoir plus sur ce passionnant projet, il y a le canal IRC
`#tetalab` sur le réseau Freenode et/ou les rencontres du mercredi
soir au DD2, à Mixart-Myrys.





