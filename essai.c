/*
 *		essai.c
 */

#include	<stdio.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<ctype.h>
#include	<string.h>
#include	<ncurses.h>
#include	<time.h>

#include	"core/utils.h"
#include	"core/sysmetrics.h"
#include	"serial/serial.h"
#include	"viz/curses/ecran.h"


int run_the_terminal(int fd_serial, char *title, WINDOW *win);	/* XXX */

int	verbosity;

/* --------------------------------------------------------------- */
int traite_les_messages(int sfd, int nbloops, int notused)
{
int		foo, key;
int		loop_forever;
char		ligne[200];
int		Idatas[4];
float		Fdatas[4];
FILE		*fp;
time_t		temps,
		old_temps = (time_t)0L;

fp = fopen("serial/foo.dat", "a");
if (NULL==fp) {
	fprintf(stderr, "*** error fopen datafile ***\n");
	return -1;
	}

if (0==nbloops) {
	fprintf(stderr, "looping forever...\n");
	loop_forever = 1;
	}
else	{
	fprintf(stderr, "looping %d rounds\n", nbloops);
	loop_forever = 0;
	}

do	{

	if (kbhit()) {
		static char valid_k[] = "+-01";
		key = getch();

		/* if it is a valid key, send it to the Arduino */
		if (NULL!=strchr(valid_k, key)) {
			foo = putbyte(sfd, key);
			fprintf(stderr, "put byte %02X -> %d\n",
						key, foo);
			}
		else if (0x14==key) {		/* request for terminal */

			foo = run_the_terminal(sfd, " terminal ", 0);
			// putbyte(sfd, 'x'); putbyte(sfd, '\r');	
			sprintf(ligne, "retour terminal = %d", foo);
			aff_message(ligne);	sleep(1);
			}
		else	{
			sprintf(ligne, "Key 0x%02X invalid ", key);
			fprintf(stderr, "%s\n", ligne);
			aff_message(ligne); sleep(1); aff_message("");
			}
		}


	/* here we are waiting for a frame from the
	   Arduino. In the future, we may have an
	   finely tunned event-synth system here */
	foo = getline_to(sfd, ligne, 100, 0);
#if DEBUG_LEVEL
	if (foo) fprintf(stderr, "get values -> %d\n", foo);
#endif

	switch (*ligne) {

	case 'M':
	case 'L':
		aff_message(ligne);
		break;

	case 'T':	
		foo = parse4_Ivalues(ligne, 'T', Idatas);
#if DEBUG_LEVEL
		if (foo) fprintf(stderr, "parse I val -> %d\n", foo); 
#endif
		values2temperature(Idatas, Fdatas);

		for (foo=0; foo<3; foo++) {
			sprintf(ligne, "%4d", Idatas[foo]);
			minidigit_affstr(stdscr, 12+(12*foo), 8, ligne);
			aff7segs_float(stdscr, 8+(12*foo), 55, Fdatas[foo]);
			}

	/* here we are loging all that temps values */

		temps = time(NULL);
		if ((temps-old_temps) > 50) {
			fprintf(fp, "%ld %f %f %f %f\n", temps,
				Fdatas[0], Fdatas[1],
				Fdatas[2], Fdatas[3]);
			fflush(fp);
			old_temps = temps;
			}
		break;			/* case 'T' */
		}

	fflush(stderr);			/* really WTF? here */

	if ( loop_forever )	nbloops = 1;

	} while (0 != nbloops);

fclose(fp);

return 0;
}
/* --------------------------------------------------------------- */
static void finish(void)
{
endwin();       
fprintf(stderr, "end of pid %d\n", getpid());
exit(0);
}
/* --------------------------------------------------------------- */
void help(int k)
{
puts("options : ");
puts("\t-d\tserial device to read.");
puts("\t-K\tset the K parameter.");
puts("\t-n NN\tnumber of loops, 0 is infinity");
puts("\t-v\tincrease verbosity.");
exit(0);
}
/* --------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		opt;
int		serial_in;
char		*device = "/dev/ttyACM0";
int		K = 0;
char		ligne[100];
int		nbreloops;

printf("\n**** %s **** compiled the %s at %s ***\n",
		argv[0], __DATE__, __TIME__);

nbreloops = 100000;

while ((opt = getopt(argc, argv, "d:hn:v")) != -1) {
	switch (opt) {
		case 'd':	device = optarg;	break;
		case 'h':	help(0);		break;
		case 'K':	K = atoi(optarg);	break;
		case 'n':	nbreloops=atoi(optarg);	break;
		case 'v':	verbosity++;		break;
		default:				break;
		}
	}

serial_in = prepare_UART(device, 9600);
if (serial_in < 0) {
	fprintf(stderr, "\n%s : open device : error %d on %s\n",
				argv[0], serial_in, device);
	exit(1);
	}

sleep(1);

initscr();
nonl();         cbreak();       noecho();
keypad(stdscr, TRUE);           /* acces aux touches 'curseur' */
atexit(finish);

sprintf(ligne, " Demonstrator pid:%d %s ", getpid(), device);
fond_ecran(ligne);


traite_les_messages(serial_in, nbreloops, K);

/*
 *      plop, on a fini, il faut restaurer la console
 */
finish();

return 0;
}

/* --------------------------------------------------------------- */
