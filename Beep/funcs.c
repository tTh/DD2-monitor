/*
 *	algua beep module
 *
 *	il faut etre honnete, mais ce module m'interpelle, parce
 *	ce que je ne comprend plus l'intention premiere.
 *
 */

#include  <stdio.h>
#include  <string.h>
#include  <unistd.h>

#include  "funcs.h"

/* --------------------------------------------------------------------- */
/*
 *	calculs sur une zone de l'image
 *	valeur retournee entre 0 et 255
 */
double niveau_zone(unsigned char *datas, int w, int h, Rect *rp)
{
unsigned char	*bptr;

int		niveau = 0;

int		lig, col;
int		l2, c2, off;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %d %p )\n", __func__, datas, w, h, rp);
fprintf(stderr, "  > x %d  y %d  w %d  h %d\n", rp->x, rp->y, rp->w, rp->h);
#endif

l2 = (rp->y+rp->h);
c2 = (rp->x+rp->w);

for (lig=rp->y; lig<l2; lig++) {
	
	off = (3*w*lig) + (3*rp->x);		/* WTF ? */
	bptr = datas + off;

	for (col=rp->x; col<c2; col++) {

		niveau += *(bptr+2);	/* add value to sum */
		bptr += 3;		/* next pixel */

		}
	}

return (double)niveau / (double)(rp->w * rp->h);
}
/* --------------------------------------------------------------------- */
