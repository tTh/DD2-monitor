# AlguaBeep

C'est une expérimentation hasardeuse pour traduire des images de webcam
en sons qui déchirent les oreilles, mais qui sont relativement
inoffensifs.

Pour le moment, il n'y a pas de procédure d'installation, il faut
donc lancer le binaire à partir du répertoire de dèv :
`~/Devel/DD2-monitor/Beep/alguabeep [options]`, ce qui implique
de ne pas aller mettre la grouille dans ce coin-là.

Ensuite, pour apprécier pleinement la kitchitude (assumée) de l'affichage
numérique de la machinerie en action, il faut un `xterm` en 150 caractères
de large, et une toute petite fonte.

## Usage

```
available options:
        -d /dev/?       select video device
        -I              take pictures
        -K              set the K parameter
        -n NNN          how many frames ?
        -p NNN          period in seconds
        -s WxH          set capture size
        -v              increase verbosity
```

### Explications

- **-d** : choix du périphérique vidéo. Les deux cas fréquents : une seule
 caméra, c'est `/dev/video0`.
 Si il y a une caméra internet et une webcam USB, la seconde sera `/dev/video2`,
 ou l'inverse, selon que l'externe est branchée au démarrage ou non.
- **-I** : dont't use !
- **-K** : useless.
- **-n** : nombre de passe, sachant qu'il y a en gros 10 à 15 images traitées
 par seconde, vous calculez la durée.
- **-p** : la période, ou l'espacement entre deux trames. Par exemple,
 _0.25_ c'est 4 images par seconde.
- **-s** : taille de l'image en pixels, le défaut est `1920x1080`.
- **-v** : demande au kulge de raconter sa vie.

Pour le moment, il n'est pas possible de choisir la sortie du son, c'est
celle qui est déclarée 'par défaut' dans le système.

### Conseils

En cas de problème sur l'acquisition vidéo, dans un terminal,
tapez `ffplay /dev/videoN`, avec `N` de zéro à quatre. Vous
tomberez peut-être sur l'image désirée.

En cas de système muet, il faut aller voir avec `alsamixer` si la sortie
par défaut n'est pas en `mute`, mais il faut aussi parfois aller négocier
avec _pulseaudio_ et sa logique étrange. Bon courage.


Et surtout que la maman des algues reste CALME !

