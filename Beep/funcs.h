/* --------------------------------------------------------------------- */
/*
 *	did you'v got funk with this funcs.h file ?
 */
typedef struct {
	int	x, y, w, h;
	} Rect;


/* --------------------------------------------------------------------- */
/*		image processing				*/

double niveau_zone(unsigned char *datas, int w, int h, Rect *rp);

/* --------------------------------------------------------------------- */
/*		display module					*/

int initialise_ecran(int type);
void finish(void);

/* --------------------------------------------------------------------- */

