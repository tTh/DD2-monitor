   /*

              A crude sound experiment, by Clairchanoir et tTh

              approimatively based on :

       V4L2 video picture grabber
       Copyright (C) 2009 Mauro Carvalho Chehab <mchehab@infradead.org>

       This program is free software; you can redistribute it and/or modify
       it under the terms of the GNU General Public License as published by
       the Free Software Foundation version 2 of the License.

       This program is distributed in the hope that it will be useful,
       but WITHOUT ANY WARRANTY; without even the implied warranty of
       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
       GNU General Public License for more details.
     */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include  <curses.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <libv4l2.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

#include  "process.h"
#include  "funcs.h"
#include  "controls.h"

struct buffer {
	void   *start;
	size_t length;
	};

int		verbosity;

/* --------------------------------------------------------------------- */
double dtime(void)
{
struct timeval	tv;
int		foo;

foo = gettimeofday(&tv, NULL);
if (foo) fprintf(stderr, "got %d in %s\n", foo, __func__);

return (double)tv.tv_sec + (double)tv.tv_usec / 1e6;
}
/* --------------------------------------------------------------- */
static void xioctl(int fh, int request, void *arg)
{
int r;

do	{
	r = v4l2_ioctl(fh, request, arg);
	} while (r == -1 && ((errno == EINTR) || (errno == EAGAIN)));

if (r == -1) {
	fprintf(stderr, "%s: error %d, %s\n", __func__, errno, strerror(errno));
	exit(EXIT_FAILURE);
	}
}
/* --------------------------------------------------------------------- */
void help(int v)
{
puts("available options:");
puts("\t-d /dev/?\tselect video device");
puts("\t-I\t\ttake pictures");
puts("\t-K\t\tset the K parameter");
puts("\t-n NNN\t\thow many frames ?");
puts("\t-p NNN\t\tperiod in seconds");
puts("\t-s WxH\t\tset capture size");
puts("\t-v\t\tincrease verbosity");
exit(0);
}
/* --------------------------------------------------------------------- */

int main(int argc, char **argv)
{
struct v4l2_format              fmt;
struct v4l2_buffer              buf;
struct v4l2_requestbuffers      req;
enum v4l2_buf_type              type;
fd_set                          fds;
struct timeval                  tv;
int                             r, fd = -1;
unsigned int                    i, n_buffers;
char                            *dev_name = "/dev/video0";
char                            out_name[256], chaine[100];
FILE                            *fout;
struct buffer                   *buffers;

int		period = 0;		/* delai entre les captures */
int		nbre_capt = 1;		/* nombre de captures */
int		opt, foo;
int		K = 100;
int		mk_img = 0;
int		width=640, height=480;

double			t_debut, t_fin;

while ((opt = getopt(argc, argv, "d:hIK:n:p:v")) != -1) {
	switch(opt) {
		case 'd':	dev_name = optarg;		break;
		case 'h':	help(0);			break;
		case 'I':	mk_img = 1;			break;
		case 'K':	K = atoi(optarg);		break;
		case 'n':	nbre_capt = atoi(optarg);	break;
		case 'p':	period = atoi(optarg);		break;
		case 's':	if (2!=sscanf(optarg, "%dx%d",
							&width, &height)) {
					fprintf(stderr, "'%s' bad\n", optarg);
					exit(1);
				}
		case 'v':	verbosity++;			break;
		}
	}


fprintf(stderr, "pid of %s is %d\n", argv[0], getpid());

fd = v4l2_open(dev_name, O_RDWR | O_NONBLOCK, 0);
if (fd < 0) {
	perror("Cannot open device");
	exit(EXIT_FAILURE);
	}

foo = set_controls(fd, 127, 127);
fprintf(stderr, "reset controls -> %d\n", foo);

foo = init_process(K);
fprintf(stderr, "init process -> %d\n", foo);

CLEAR(fmt);
fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
fmt.fmt.pix.width       = 1920;
fmt.fmt.pix.height      = 1080;
fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;
xioctl(fd, VIDIOC_S_FMT, &fmt);
if (fmt.fmt.pix.pixelformat != V4L2_PIX_FMT_RGB24) {
	printf("Libv4l didn't accept RGB24 format. Can't proceed.\n");
	exit(EXIT_FAILURE);
	}

if ((fmt.fmt.pix.width != 640) || (fmt.fmt.pix.height != 480)) {
	fprintf(stderr, "Warning: driver is sending image at %dx%d\n",
			    fmt.fmt.pix.width, fmt.fmt.pix.height);
	}

CLEAR(req);
req.count = 2;
req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
req.memory = V4L2_MEMORY_MMAP;
xioctl(fd, VIDIOC_REQBUFS, &req);

buffers = calloc(req.count, sizeof(*buffers));
for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
	CLEAR(buf);

	buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory      = V4L2_MEMORY_MMAP;
	buf.index       = n_buffers;

	xioctl(fd, VIDIOC_QUERYBUF, &buf);

	buffers[n_buffers].length = buf.length;
	buffers[n_buffers].start = v4l2_mmap(NULL, buf.length,
				PROT_READ | PROT_WRITE, MAP_SHARED,
				fd, buf.m.offset);

	if (MAP_FAILED == buffers[n_buffers].start) {
		perror("mmap");
		exit(EXIT_FAILURE);
		}
	}

for (i = 0; i < n_buffers; ++i) {
	CLEAR(buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	buf.index = i;
	xioctl(fd, VIDIOC_QBUF, &buf);
	}

if (nbre_capt)	fprintf(stderr, "running for %d frames\n", nbre_capt);
else		fprintf(stderr, "just one shoot...\n");
t_debut = dtime();

foo = lancement_thread_son(0);
if (foo) {
	fprintf(stderr, "erreur %d lancment thread son\n", foo);
	exit(1);
	}

initialise_ecran(0);

type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
xioctl(fd, VIDIOC_STREAMON, &type);

sprintf(chaine, "entering main loop, %9d iters\n", nbre_capt);
mvaddstr(1, 0, chaine);
refresh();
fprintf(stderr, "%s\n", chaine);


for (i = 0; i < nbre_capt; i++) {
	do	{
		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		/* Timeout. */
		tv.tv_sec = 2;
		tv.tv_usec = 0;

		r = select(fd + 1, &fds, NULL, NULL, &tv);
		} while ((r == -1 && (errno = EINTR)));

	if (r == -1) {
		perror("select");
		return errno;
		}

	CLEAR(buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	xioctl(fd, VIDIOC_DQBUF, &buf);
#if DEBUG_LEVEL > 1
	fprintf(stderr, "buf.bytesused %d\n", buf.bytesused);
#endif

	if ( period!=0 || 0==(i%10) ) {
		sprintf(chaine, "frame %d  %8.1fs", i, dtime()-t_debut);
		mvaddstr(1, 50, chaine);
		}

	foo = charcuteur(buffers[buf.index].start, 
			fmt.fmt.pix.width, fmt.fmt.pix.height, K);

//	foo = process(buffers[buf.index].start, 
//			fmt.fmt.pix.width, fmt.fmt.pix.height, K);

	if (mk_img) {
		sprintf(out_name, "o/out%04d.ppm", i);
		if (verbosity) fprintf(stderr, "\t--> %s\n", out_name);

		fout = fopen(out_name, "w");
		if (!fout) {
			perror(out_name);
			exit(EXIT_FAILURE);
			}
		fprintf(fout, "P6\n%d %d 255\n",
			    fmt.fmt.pix.width, fmt.fmt.pix.height);
		fwrite(buffers[buf.index].start, buf.bytesused, 1, fout);
		fclose(fout);
		}

	/* XXX CRITICAL BUG XXX
		Doc say 'period can be real', so why not
			use nanosleep(2) ?
	*/
	if (nbre_capt > 1 && period) {
		sleep(period);
		}

	xioctl(fd, VIDIOC_QBUF, &buf);
}

t_fin = dtime();

if (nbre_capt > 0) {
	fprintf(stderr, "%f frames per second\n",
		(double)nbre_capt/(t_fin-t_debut));
	}

type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
xioctl(fd, VIDIOC_STREAMOFF, &type);
for (i = 0; i < n_buffers; ++i) {
	v4l2_munmap(buffers[i].start, buffers[i].length);
	}

endwin();
v4l2_close(fd);

return 0;
}
