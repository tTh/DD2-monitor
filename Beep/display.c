/*
 *	un module 'curses' de AlguaBeep
 */

#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>

#include  <curses.h>

#include  "funcs.h"

/* ---------------------------------------------------------------- */
int initialise_ecran(int type)
{
char	chaine[100];

initscr();
nonl();         cbreak();       noecho();
keypad(stdscr, TRUE);           /* acces aux touches 'curseur' */

atexit(finish);

sprintf(chaine, " AlguaBeep (%s %s) pid=%d \n", __DATE__, __TIME__,
						getpid());
standout(); mvaddstr(0, 0, chaine); standend();

refresh();

fprintf(stderr, "%s: COLS=%d LINES=%d\n", __func__, COLS, LINES);

return 0;
}
/* ---------------------------------------------------------------- */
void finish(void)
{
endwin();       
fprintf(stderr, "end of pid %d\n", getpid());
exit(0);
}

/* ---------------------------------------------------------------- */
/* ---------------------------------------------------------------- */
