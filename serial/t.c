/*
 *	Experiments with the serial input 
 *	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

/**********************************************************************
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
**********************************************************************/

#include  <stdio.h>
#include  <stdlib.h>
#include  <strings.h>
#include  <ctype.h>
#include  <time.h>
#include  <unistd.h>			//Used for UART
#include  <fcntl.h>			//Used for UART
#include  <errno.h>
#include  <termios.h>			//Used for UART
#include  <getopt.h>

#include  "serial.h"

int	verbosity;


/* ---------------------------------------------------------------- */
int read_temps(char *buff, int k)
{
float		datas[4];
int		raw[4], foo;
long		temps;

foo = parse4_Ivalues(buff, 'T', raw);
	// 
if (foo >= 0) {
	temps = time(NULL);
	values2temperature(raw, datas);
	printf("%ld %f %f %f %f\n", temps,
			datas[0], datas[1], datas[2], datas[3]);
	fflush(stdout);
	}
else	{
	fprintf(stderr, "%s: parse -> %d\n", __func__, foo);
	}

return 0;
}
/* ---------------------------------------------------------------- */
int read_light(char *buff, int k)
{
int		val;

if (1 == sscanf(buff+1, "%d", &val)) {
	printf("--------- %04X ----------\n", val);
	return 0;
	}
return -3;
}
/* ---------------------------------------------------------------- */

int loop(int sfd, int iters)
{
int		count, foo;
long		temps;
char		ligne[200], buff[200];
struct tm       *p_tms;

for (count=0; count<iters; count++) {
	//
	foo = getline_to(sfd, ligne, 100, 0);
	//
	if (verbosity) {
		/* fprintf(stderr, "getline #%d on %d -> %d\n",
					count, iters, foo); */
		fprintf(stderr, "%s\n", ligne);
		}
	//
	if (verbosity > 1)
		{
		p_tms = localtime(&temps);
		(void)strftime(buff, 19, "%H:%M:%S", p_tms);
		fprintf(stderr, "%s    %6d / %d\n", buff, count, iters);
		}

	foo = 0;
	switch (ligne[0]) {
		case 'L':
			foo = read_light(ligne, 0);
			break;
		case 'T':
			foo = read_temps(ligne, 0);
			break;

		default:
			fprintf(stderr, "*** WTF '%s' ?\n", ligne);
			foo = -1;
			break;
			
		}
		
	if (foo) {
		fprintf(stderr, "%s : error %d after switch\n",
					__func__, foo);
		}
	if (verbosity)		puts("");
	
	}
return 0;
}
/* ---------------------------------------------------------------- */
void help(int k)
{
puts("options : ");
puts("\t-d\tserial device to read.");
puts("\t-K\tset the K parameter.");
puts("\t-n\tnumber of records to grab.");
puts("\t-t\tselect the function");
puts("\t-v\tincrease verbosity.");
}
/* ---------------------------------------------------------------- */
int main (int argc, char *argv[])
{
int		serial_in;
char		*device = "/dev/ttyS0";
int		nbre, speed, opt, K=0;
int		param, retv;

/* set some default values */
verbosity	= 0;
nbre		= 25;
speed		= 9600;
param		= 1;

while ((opt = getopt(argc, argv, "d:n:vh")) != -1) {
	switch (opt) {
		case 'p':	param = atoi(optarg);	break;
		case 'v':	verbosity++;		break;
		case 'K':	K = atoi(optarg);	break;
		case 'n':	nbre = atoi(optarg);	break;
		case 'd':	device = optarg;	break;
		case 'h':	help(0);		exit(0);
		default:
			fprintf(stderr, "%s : uh ?", argv[0]);
			exit(1);
			break;
		}

	}

if (verbosity) {
	fprintf(stderr, "Testing Serial Software - compiled "	\
				__DATE__ " " __TIME__ "\n");
	}

serial_in = prepare_UART(device, speed);
if (serial_in < 0) {
	fprintf(stderr, "%s : open device : error %d on %s\n",
				argv[0], serial_in, device);
	exit(1);
	}

fprintf(stderr, "going to listen on %d\n", serial_in);

switch (param) {
	case 0:
		retv = loop(serial_in, nbre);
		break;
	case 1:
		retv = -1;
		break;
	default:
		retv = -2;
		break;
	}

fprintf(stderr, "Returned value is %d\n", retv);

return 0;
}

/* ---------------------------------------------------------------- */
