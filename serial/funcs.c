
#include  <stdio.h>
#include  <string.h>

#include  "serial.h"

extern int	verbosity;

/* ---------------------------------------------------------------- */
/*
 *	compute the integer mean value of a four values
 *	tagged lines.
 */
int parseXvalue(char *line, char cflag)
{
int	value, foo;
int	vrd[4];

value=0;

if ( cflag != *line ) {
	if (verbosity) {
		fprintf(stderr, "%s : line[0] 0x%x bad\n",
					__func__, *line);
		}
	return -777;
	}

foo = sscanf(line+1, "%d %d %d %d", vrd, vrd+1, vrd+2, vrd+3);
#if DEBUG_LEVEL
fprintf(stderr, "%s : sscanf -> %d\n", __func__, foo);
#endif
if (4 != foo) {
	return -666;
	}
for (foo=0; foo<4; foo++) {
	value += vrd[foo];
	}

value /= 4;

return value;
}
/* ---------------------------------------------------------------- */
/*
 *   this fonction is specific to the LM35 thermo-sensor
 *   connected to a ADC pin of an Arduino Mega
 *
 *	WARNING !
 *   this function _must_ be modofied if you change the
 *   Vref of the Analog to Digital converter on the
 *   Arduino !
 *
 */
int values2temperature(int raw[4], float array[4])
{
int	foo;
for (foo=0; foo<4; foo++) {
 	array[foo] = (float)raw[foo] * (1.1 / 1023.0 * 100.0);
	}
return 0;
}
/* ---------------------------------------------------------------- */
int parse4_Ivalues(char *line, char cflag, int array[4])
{
int		foo, raw[4];

if ( cflag != *line ) {
	if (verbosity) {
		fprintf(stderr, "%s : line[0] 0x%x bad\n",
					__func__, *line);
		}
	return -776;
	}

foo = sscanf(line+1, "%d %d %d %d", raw, raw+1, raw+2, raw+3);
if (4 != foo) {
	fprintf(stderr, "%s : sscanf -> %d\n", __func__, foo);
	return -665;
	}
	
memcpy(array, raw, 4*sizeof(int));

return 4;
}
/* ---------------------------------------------------------------- */

int parse4_Fvalues(char *line, char cflag, float array[4])
{
float		ftmp[4];
int		foo;

if ( cflag != *line ) {
	if (verbosity) {
		fprintf(stderr, "%s : line[0] 0x%x bad\n",
					__func__, *line);
		}
	return -777;
	}

foo = sscanf(line+1, "%f %f %f %f", ftmp, ftmp+1, ftmp+2, ftmp+3);
if (4 != foo) {
	fprintf(stderr, "%s : sscanf -> %d\n", __func__, foo);
	return -666;
	}
// fprintf(stderr, "\tV %f\n", ftmp[0]);

memcpy(array, ftmp, 4*sizeof(float));

return 4;
}
/* ---------------------------------------------------------------- */
