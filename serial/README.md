# Serial Input

But premier de ce module : recevoir les données fournies par l'automate
de contrôle du phytotron.

Ayant déja pratiqué ce genre de chose (recevoir des données par rs232)
pour un déja ancien
[projet artsitique](http://art.dinorama.fr/bdf/) conçu par et
avec _MadPhoenix_, je me propose de reprendre quelques parties de ce code,
de le remettre au gout du jour et de le tester dès que possible.

## principe général

Pour écouter plusieurs lignes simultanément, chaque port sera traité
par un _thread_ séparé, et les diverses données reçues seront pré-traitées
par celui-ci. Les flux de données seront alors agrégées par
le célèbre *synthétiseur d'évènement* mis au point il y a très longtemps
par le professeur Cispeo.

## À venir...

Un petit exemple ?

Oui, voilà. À ce jour (20 déc. 2018), on va dire que ça ne marche pas.
Il faut dire que les `serial devices` ont toujours étés un peu le
domaine de la magie noire. Mais quand même, coincer sur un `read` qui
ne bloque pas, c'est un peu ironique.

Après un peu plus d'investigation, j'en arrive à conclure qu'il y a
plein de subtilités entre les diverses variantes d'Arduino. Mais pas que.
Je pense que les quatre ports série supplémentaires de l'Arduino Mega
seront moins capricieux.



 















