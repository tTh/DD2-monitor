/*
 *		serial.h
 *		--------
 *
 */

int prepare_UART(char *port, int bauds);

int getbyte(int fd);				/* really brotched func */
int putbyte(int fd, unsigned char byte);



/* timeout is exprimed in milliseconds. */
int getbyte_to (int fd, int to_ms);

int getline_to(int fd, char *where, int szm, int to_ms);



/* auxiliary and test functions */

int parseXvalue(char *asciidatas, char id);
int values2temperature(int raw[4], float array[4]);

int parse4_Ivalues(char *line, char cflag,   int array[4]);
int parse4_Fvalues(char *line, char cflag, float array[4]);


