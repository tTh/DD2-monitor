#!/bin/bash

DEVICE="/dev/ttyACM0"
DATAFILE="foo.dat"
TMPFILE="/tmp/dd2data"

IMAGE="graphe.png"
NB_READ=500

./t -n ${NB_READ} -d ${DEVICE} | tee -a ${DATAFILE}

gnuplot << __EOC__
set term png 	size 3200,640
set output 	"${IMAGE}"
set grid
set title	"Temperature dans le Double Dragon 2"
set xdata 	time
set timefmt 	"%s"
set format x 	"%d, %H:%M"
set yrange [ 0.0 : 30.0 ]
plot	"${DATAFILE}" using 1:2 title " foo" with lines, \
	"${DATAFILE}" using 1:3 title " bar" with lines, \
	"${DATAFILE}" using 1:4 title "quux" with lines, \
	"${DATAFILE}" using 1:5 title "booz" with lines
__EOC__

# display ${IMAGE}


