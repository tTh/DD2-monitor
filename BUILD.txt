		+--------------------------------+
		| how to build the dd2-monitor ? |
		+--------------------------------+

First step, build some parts of the core library :

	$ cd core
	$ make t
	$ ./t -v
	$ cd ..

Then you must have datas for working on. One source of datas
is the four values who came from the Arduino over serial line.
At this time, you must look at the configuration file.

	$ cd serial
	$ make t

Have a look at the 'essai.sh' file for an example of simple
usage of the serial input. For generating all that numbers,
you have to run some code on the Arduino mega. Sources are
in the simulator/ folder.




