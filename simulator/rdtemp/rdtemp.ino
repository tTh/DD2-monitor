/*
 *          Phytotron
 * lecture des capteurs de temperature LM35
 */
/* -------------------------------------------------- */

#define NBVAL   4
#define DELAI   1000

#define RELAIS_FRIGO    42
#define RELAIS_VENTILO  40

#define prt(a)      Serial.print(a)
#define prtln(a)    Serial.println(a)


typedef struct {
  unsigned short    magic;
  char              tag[4];
  short             delai;
  short             temp_maxi;
  short             temp_mini;
  short             control;
  } Global;

Global parametres;

/* -------------------------------------------------- */
void setup() {
short foo;

  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
 
  pinMode(RELAIS_FRIGO, OUTPUT);
  pinMode(RELAIS_VENTILO, OUTPUT);
  
  Serial.print("M booting\n");  

  for (foo=0; foo<10; foo++) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(50);
    digitalWrite(LED_BUILTIN, LOW);
    delay(50);
    }

  /* XXX */
  /*  changing the voltage reference of the ADC
   *  greatly increase the prcision on the limited
   *  range of our temperatures.
   */
  analogReference(INTERNAL1V1); // Pour Arduino Mega2560
  read_config(0, &parametres);
  // display_config(&parametres);  
  Serial.print("M running now\n");
  
}
/* ================================================== */
/*
 * the light captor is on A4 pin
 */
void readlight(void)
{
  short value;
  delay(100);
  value = analogRead(A4);
  Serial.print("L ");
  Serial.print(value);
  Serial.print('\n');
}
/* ================================================== */
short adc_pins[] = { A0, A1, A2, A3 };
#define NB_PASSE  4
/* -------------------------------------------------- */
void updatevalues(short *ptr)
{
  short foo, pass;

  for (foo=0; foo<NBVAL; foo++) { ptr[foo] = 0; }
  
  digitalWrite(LED_BUILTIN, HIGH);
  for (pass=0; pass<NB_PASSE; pass++) {
    for (foo=0; foo<NBVAL; foo++) {
      ptr[foo] += analogRead(adc_pins[foo]);
      delay(50);
      }
    }
  for (foo=0; foo<NBVAL; foo++) { ptr[foo] /= NB_PASSE; }
  digitalWrite(LED_BUILTIN, LOW);
}
/* -------------------------------------------------- */
void sendvalues(short *ptr)
{
  int foo;

  prt("T");
  for (foo=0; foo<NBVAL; foo++) {
    prt(" ");
    prt(ptr[foo]);
    }
  prt('\n');
}
/* -------------------------------------------------- */
void update_and_send(short *pvalues)
{
  
  updatevalues(pvalues);
  sendvalues(pvalues);
 
}
/* ================================================== */
void loop() {
  static int foo = 0;
  int         key;
  short values[NBVAL], temp_interne;

  
  update_and_send(values);

  temp_interne = values[1];
  if (temp_interne < parametres.temp_mini) {
    prtln("M temp is low");
    delay(500);
    controle_frigo(0);
    }
  else if (temp_interne > parametres.temp_maxi){
    prtln("M temp is hight");
    delay(500);
    controle_frigo(1);
    }


  if (foo++ > 5) {
    readlight();    foo = 0;
    }

 

 /* check for external request */
 if (Serial.available()) {
    key = Serial.read();
    switch (key) {
      case 'U':
          prtln("M cli request");
          phytocli();
          break;
      case '+':           /* allume le frigo */
          controle_frigo(1);
          break;
      case '-':           /* eteint le frigo */
          controle_frigo(0);
          break;
      case '1':           /* allume le ventilo */
          controle_ventilo(1);
          break;
      case '0':           /* eteint le ventilo */
          controle_ventilo(0);
          break;
      default:
          prt("M "); prt(key);
          prtln(" bad control code");
          break;          
      }
 }


 
 delay(parametres.delai);

 /* ETERNAL LOOP END HERE */
}

/* -------------------------------------------------- */
