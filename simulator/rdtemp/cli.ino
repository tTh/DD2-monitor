/*
 *          Phytotron
 * configuration par la console
 */
/* -------------------------------------------------- */

#include  <stdlib.h>
#include  <string.h>

#define DEBUG 0
/* -------------------------------------------------- */
short validate_config(Global *param)
{
short delta;
  
if (param->magic != 0xfde9) {
  prtln("bad magic");
  return 1;
  }
if (param->delai < 100) {
  prt(param->delai); prtln("delay too short");
  return 2;
  }
delta = param->temp_maxi - param->temp_mini;
if (delta < 20) {
  prt(delta); prtln(" bad delta");
  return 3;
  }
return 0;
}
/* -------------------------------------------------- */
char waitkey(char echo)
{
char    key;
while (!Serial.available());
key = Serial.read();
if (echo) Serial.write(key);
return key;
}
/* -------------------------------------------------- */
short readline(char *where, short sz)
{
char  key;
short  count = 0;
for(;;)  {
    /* check for end of buffer */
    if (count==sz) {
      where[count] = '\0';
      return -1;
      }
    key = waitkey(1);
#if DEBUG > 1
    prtln((int)key);
#endif

    if ((0x08==key) && count) {
      /* we have got a backspace */
      count--;
      where[count] = '\0';
#if DEBUG
      prt("< "); prtln(count);
#endif
      continue;
      }
    if ('\r' == key) {
      prt('\n');
      where[count] = '\0';
      return count;
      }
    where[count++] = key;
    }
/* NOTREACHED */
}
/* --------------------------------------------------------------- */
void hexdump(unsigned char *ptr, short nbre)
{
short    foo;
for (foo=0; foo<nbre; foo++) {
    Serial.print(ptr[foo], HEX);
    Serial.print(' ');
    }
prtln("");
}
/* --------------------------------------------------------------- */
static void clihelp()
{
 prtln("x\texit cli");
 prtln("I\tinit storage");        /* dangerous !!! */
 prtln("s d N\tset delay");
 prtln("s l N\tset lo temp");
 prtln("s h N\tset hi temp");
 prtln("d\tdisplay config");
 prtln("r\tread config");
 prtln("v\tvalidate config");
 prtln("w\twrite config");
 prtln("T\ttest relay");
 prtln("+/-\tcontrole frigo");
 prtln("h\thexdump config");
}
/* --------------------------------------------------------------- */
void setvalue(char *cmdline, Global *param)
{
char    key, *sptr, *vptr;
short   value;

// hexdump((unsigned char *)cmdline, 10); 
key = *(sptr = strtok(NULL, " "));
// hexdump((unsigned char *)cmdline, 10);
vptr = strtok(NULL, " ");
value = atoi(vptr);

prt(key); prt(' '); prtln(value);

switch(key) {
  case 'd':     param->delai = value;         break;
  case 'h':     param->temp_maxi = value;     break;
  case 'l':     param->temp_mini = value;     break;

  default:    prtln("GNI ?");                 break;
  }

}
/* --------------------------------------------------------------- */
/*
 *  use this function with caution, it can burn your system !
 */
void test_relais(short nb) {
short foo;
prtln("test du relais frigo");

for (foo=0; foo<nb; foo++) {    
  controle_frigo(1); 
  delay(parametres.delai);
             
  controle_frigo(1);  
  delay(parametres.delai);
  }
}
/* --------------------------------------------------------------- */
void controle_frigo(char on_off)
{
if (on_off) {
  prtln("M frigo ON");
  digitalWrite(RELAIS_FRIGO, HIGH);
  }
else {
  prtln("M frigo OFF");
  digitalWrite(RELAIS_FRIGO, LOW);
  }
}
/* --------------------------------------------------------------- */
void controle_ventilo(char on_off)
{
if (on_off) {
  prtln("M ventilo ON");
  digitalWrite(RELAIS_VENTILO, HIGH);
  }
else {
  prtln("M ventilo OFF");
  digitalWrite(RELAIS_VENTILO, LOW);
  }
}
/* --------------------------------------------------------------- */

void phytocli(void)
{
char     flag_exit = 0;
char     ret, key;

#define TLINE 40
char     line[TLINE+1], *sptr;
static char separators[] = " \t";

prtln("entering cli, '?' to help, 'x' to quit.");   

do  {
  prt("$ ");
  ret = readline(line,TLINE);
#if DEBUG > 1
  prt("readline -> "); hexdump((unsigned char *)line, ret);
#endif
  key = *(sptr = strtok(line, " "));

  switch(key) {
    case 'x':  flag_exit=1;                            break;
    case '?':  clihelp();                              break;
    case 'I':  init_storage(&parametres);              break;
    case 'd':  display_config(&parametres);            break;
    case 'r':  read_config(0, &parametres);            break;
    case 'w':  write_config(0, &parametres);           break;
    case 'h':  hexdump((unsigned char *)&parametres,
                            sizeof(Global));           break;
    case 's':  setvalue(line, &parametres);            break;
    
    case 'v':   validate_config(&parametres);          break;

    case '+':   controle_frigo(1);                     break;
    case '-':   controle_frigo(0);                     break;
    case '1':   controle_ventilo(1);                   break;
    case '0':   controle_ventilo(0);                   break;
    case 'T':   test_relais(5);                        break; 
   
    default:    prtln("gni ?");                        break;
    }
  
  } while (!flag_exit);

}
/* --------------------------------------------------------------- */
