/* --------------------------------------------------------------- */
#include <EEPROM.h>
/* --------------------------------------------------------------- */
short init_storage(Global *what)
{
short     foo;

Serial.print("eeprom length: ");
Serial.println(EEPROM.length());
Serial.print("global length: ");
Serial.println(sizeof(Global));

memset(what, 0, sizeof(Global));
what->magic = 0xfde9;
EEPROM.put(0, *what);

return 0;
}
/* --------------------------------------------------------------- */
short read_config(short num, Global *where)
{
unsigned short magic;
  
#if DEBUG
prtln(">>> read config");
#endif
magic = 0;

EEPROM.get(0, magic);
prt("M magic is "); prtln(magic);
if (0xfde9 != magic) return -1;
EEPROM.get(0, *where);
return -2;
}
/* --------------------------------------------------------------- */
short write_config(short num, Global *from)
{
from->magic = 0xfde9;
memcpy(from->tag, "aaaa", 4);
from->control++;

EEPROM.put(0, *from);

/* no check of good write here ? */

return -1;
}
/* --------------------------------------------------------------- */
short display_config(Global *what)
{
char    foo, c;
#if DEBUG
prtln(">>> display config");
#endif

prt("Magic     : ");    prtln(what->magic);
prt("Id        : ");
for (foo=0; foo<4; foo++) {
  if (isprint(c=what->tag[foo]))  prt(c);
  else                            prt('?');
  }
prtln("");
prt("Delay     : ");    prtln(what->delai);
prt("Temp mini : ");    prtln(what->temp_mini);
prt("Temp maxi : ");    prtln(what->temp_maxi);
prt("Control   : ");    prtln(what->control);
return -1;
}
/* --------------------------------------------------------------- */
