/*
 *  simulateur de telemesure automate
 */

/* -------------------------------------------------- */

#define NBVAL   4
#define DELAI   1789

int   values[NBVAL];

/* -------------------------------------------------- */
void setup() {
  int   foo;
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  for (foo=0; foo<NBVAL; foo++) {
    values[foo] = 0;
  }
}
/* -------------------------------------------------- */
void updatevalues(void)
{
  int foo;
  for (foo=0; foo<NBVAL; foo++) {
    if (rand()%100<42) {
      values[foo] += (foo + 1);
      }
    if (values[foo] > 1023) {
      values[foo] = rand()%5;
      }
  }
}
/* -------------------------------------------------- */

void sendvalues(void)
{
  int foo;
  char ligne[100];

  sprintf(ligne, "X %d %d %d %d",
        values[0], values[1],values[2],values[3]);

  Serial.println(ligne);
}  
/* -------------------------------------------------- */

void loop() {
  updatevalues();
  sendvalues();
  delay(DELAI);
}

/* -------------------------------------------------- */

