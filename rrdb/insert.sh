#!/bin/bash

source ./commun.sh

ctime=$(date +'%s')
value=$(cut -d ' ' -f 1 /proc/loadavg)

# display and write value to a file
echo ${ctime} ${value} | tee -a bar.dat

# inject value in the rrdb file 
rrdtool	update $RRDB ${ctime}:${value}


