#!/bin/bash

#
#	creating the test database
#

source ./commun.sh

starttime=$(date +'%s')

echo creating $RRDB at ${starttime}s since epoch

rrdtool	create $RRDB			\
	--start $starttime		\
	--step 60			\
	DS:value:GAUGE:150:0:10		\
	RRA:AVERAGE:0.5:1:60



		