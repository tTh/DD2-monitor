#!/bin/bash

source ./commun.sh

tmpf="somevalues.dat"

rrdtool fetch $RRDB LAST			\
	--start 0				|
tr -d ':'					|
awk	'
	(!/nan/ && NF==2) { print $1, $2 }
	' 					\
			> ${tmpf}

#
# as an example, we are gnuploting our datas
#
gnuplot << __EOC__
set term png size 1024,512
set output "graphe.png"
set grid
set xdata time
set timefmt "%s"
set format x "%m/%d\n%H:%M"
plot "${tmpf}" using 1:2 with lines
__EOC__

# rm ${tmpf}

