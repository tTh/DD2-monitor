#!/bin/bash

source ./commun.sh

rrdtool graph value.png			\
	--start 0 --end now		\
	-w 800 -h 600			\
	DEF:value=${RRDB}:value:LAST	\
	LINE1:value#0000FF


