#!/usr/bin/awk -f

BEGIN	{
	flag_debut = 1;
	lastminute = 0;
	cumul      = 0.0;
	compte     = 0;
	}

# iterate over all the input lines
	{
	if (flag_debut) {
		debut = $1
		flag_debut = 0
		}
	minutes = int(($1-debut) / 60);
	if (minutes == lastminute) {
		val = ($2 + $3 + $4 + $5);
		cumul += val;
		compte += 4;
		}
	else	{
		val = cumul /compte;	
		print minutes, val;
		lastminute = minutes;
		cumul = 0;
		compte = 0;
		}
	}





