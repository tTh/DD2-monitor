#!/bin/bash

INFILE="../../serial/foo.dat"
NBLINES=1600
TMPFILE="/tmp/dd2data.$$"
IMAGE="av4v-m.png"

tail -${NBLINES} ${INFILE} | ./average4v.awk > ${TMPFILE}

gnuplot << __EOC__
set term png 	size 1280,420
set output 	"${IMAGE}"
set grid
set title	"Average on the last ${NBLINES} samples"
set xlabel	"Minutes"
set ylabel 	"Température"
set yrange	[ 5.0 : 30.0]
plot "${TMPFILE}" with lines
__EOC__

tail -20 ${TMPFILE}

