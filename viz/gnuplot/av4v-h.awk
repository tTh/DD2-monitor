#!/usr/bin/awk -f

BEGIN	{
	flag_debut = 1;
	lasthour   = 0;
	cumul      = 0.0;
	compte     = 0;
	}

# iterate over all the input lines
	{
	if (flag_debut) {
		debut = $1
		flag_debut = 0
		}
	heures = int(($1-debut) / 3600);
	if (heures == lasthour) {
		val = ($2 + $3 + $4 + $5);
		cumul += val;
		compte += 4;
		}
	else	{
		val = cumul /compte;	
		print $1, val;
		lasthour = heures;
		cumul = 0;
		compte = 0;
		}
	}





