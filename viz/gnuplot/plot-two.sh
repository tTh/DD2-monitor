#!/bin/bash

INFILE="../../serial/foo.dat"
NBLINES=60000
TMPFILE="/tmp/dd2data.$$"
IMAGE="av4v-h.png"

tail -${NBLINES} ${INFILE} | ./av4v-h.awk > ${TMPFILE}

gnuplot << __EOC__
set term png 	size 1280,420
set output 	"${IMAGE}"
set grid
set title	"Hourly average on the last ${NBLINES} samples"
set xlabel	"Heures"
set ylabel 	"Temperature"
set yrange	[ 0.0 : 30.0]

set xdata 	time
set timefmt 	"%s"
set format x 	"%d, %H:%M"

plot "${TMPFILE}" using 1:2 title "celcius" with lines
__EOC__

cat -n ${TMPFILE} | tail -20

