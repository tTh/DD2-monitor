/*
 *		DD2 Monitoring
 *
 *	ncurses linear vumeter display
 */

/**********************************************************************
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
**********************************************************************/

#include  <stdio.h>
#include  <stdlib.h>
#include  <strings.h>
#include  <getopt.h>
#include  <ncurses.h>

#include  "ecran.h"

extern int	verbosity;

/* ---------------------------------------------------------------- */
int vumetre_0(WINDOW *win, int lig, int col, float val, int larg)
{
int		foo, posc, c;
char		ligne[100];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %d %f %d )\n",
		__func__, win, lig, col, val, larg);
#endif

posc = (int)(val * (float)(larg-5));

sprintf(ligne, "%6.3f", val);
mvwaddstr(win, lig, 0, ligne);

for (foo=0; foo<larg; foo++) {
	c = col + foo + 2;
	if (foo<posc) {
		wstandout(win);
		mvwaddch(win, lig, c, '#');
		wstandend(win);
		}
	else	{
		mvwaddch(win, lig, c, ' ');
		}
	if (!(foo%4)) {
		mvwaddch(win, lig-1, c, '\\');
		mvwaddch(win, lig+1, c, '/');
		}
	}
wrefresh(win);

return 0;
}
/* ---------------------------------------------------------------- */
int vumetre_1(WINDOW *win, int lig, int col, float val, int larg)
{
int		foo, posc;
char		ligne[100];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %d %f %d )\n",
		__func__, win, lig, col, val, larg);
#endif

posc = (int)(val * (float)larg);

sprintf(ligne, "%6.3f", val);
mvwaddstr(win, lig, 2, ligne);

for (foo=0; foo<larg; foo++) {
	if (foo < posc) {
		wstandout(win);
		mvwaddch(win, lig, col+foo, '|');
		wstandend(win);
		}
	else	{
		mvwaddch(win, lig, col+foo, '|');
		}
	if (foo & 2) {
		mvwaddch(win, lig-1, col+foo, '|');
		mvwaddch(win, lig+1, col+foo, '|');
		}
	}
wrefresh(win);

return 0;
}
/* ---------------------------------------------------------------- */
/* ---------------------------------------------------------------- */

