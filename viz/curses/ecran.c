#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <sys/utsname.h>

#include  <getopt.h>
#include  <ncurses.h>


#include  "ecran.h"

extern int	verbosity;

/* ---------------------------------------------------------------- */
void bordure(WINDOW * w, char *texte, int type)
{
if (type)
	box(w, 0, 0);
else
	wborder(w, '|', '|', '-', '-', '+', '+', '+', '+');
wstandout(w); mvwaddstr(w, 0, 3, texte); wstandend(w);
wrefresh(w);
}
/* ---------------------------------------------------------------- */
int kbhit(void)
{
int		r, ch;

nodelay(stdscr, TRUE);
noecho();

    // check for input
ch = getch();
if( ch == ERR) {     // no input
	r = FALSE;
	}
else	{                // input
	r = TRUE;
	ungetch(ch);
	}

    // restore block and echo
echo();
nodelay(stdscr, FALSE);

return r;
}
/* ---------------------------------------------------------------- */
int aff_message(char *txt)
{
static	int	pass = 0;

#if TRACE
fprintf(stderr, "%s [%s]\n", __func__, txt);
#endif

standout();
mvhline(LINES-1, 0, ' ', COLS);
mvaddch(LINES-1, 0, "\\|/-"[(pass++)%4]);
mvaddstr(LINES-1, 2, txt);
standend();
refresh();
return 0;
}
/* ---------------------------------------------------------------- */
void barre_inverse(char c, int ligne)
{
int     foo;
standout();
for (foo=0; foo<COLS; foo++)
        mvaddch(ligne, foo, c);
standend();
/* refresh(); */
}
/* ---------------------------------------------------------------- */
/*	make display on the standard screen (stdscr) */
int fond_ecran(char *title)
{
char			*tp;
struct utsname		utsn;
int			foo;
char    		buffer[200];

tp = " DD2 Monitoring by tTh 2019 ";
if (NULL != title)	tp = title;

barre_inverse(' ', 0);
standout();
mvaddstr(0, 2, tp);

if (verbosity) {
	sprintf(buffer, " ecr: %dx%d ", COLS, LINES);
	fprintf(stderr, "%s ==> %s\n", __func__, buffer);
	foo = strlen(buffer);
	mvaddstr(0, COLS-2-foo, buffer);
	}

/* get and display hostname */
foo = uname(&utsn);
if ( !foo )	{
	mvaddstr(0, 2+strlen(tp), "on");
	mvaddstr(0, 5+strlen(tp), utsn.nodename);
	}

standend();
refresh();

return 0;
}
/* ---------------------------------------------------------------- */
