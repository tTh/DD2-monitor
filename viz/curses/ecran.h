/*
 *	interface ncurses pour dd2 monitoring
 */

int kbhit(void);			/* experimental */

int fond_ecran(char *titre);
int aff_message(char *);
void bordure(WINDOW * w, char *texte, int type);


int aff7segs_base(WINDOW * win, int lig, int col, int bits, int k);
int aff7segs_digit(WINDOW * win, int lig, int col, char digit);
int aff7segs_short(WINDOW * win, int lig, int col, short value);
int aff7segs_float(WINDOW * win, int lig, int col, float value);


int minidigit_0(WINDOW *win, int lig, int col, char digit, int k);
int minidigit_affstr(WINDOW *win, int lig, int col, char *str);
int minidigit_HMS(WINDOW *win, int lig, int col, int k);


WINDOW * open_waterfall(char *title, int flags);
int plot_waterfall(WINDOW *wf, int flags, float values[4]);
int close_waterfall(WINDOW *wf, int notused);


int vumetre_0(WINDOW * win, int lig, int col, float val, int larg);
int vumetre_1(WINDOW * win, int lig, int col, float val, int larg);
