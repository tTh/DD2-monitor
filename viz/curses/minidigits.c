/*
 *		DD2 Monitoring
 *
 *	mini digits
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include	<time.h>
#include  <ctype.h>
#include  <getopt.h>
#include  <ncurses.h>

#include  "ecran.h"

extern int	verbosity;

/* ---------------------------------------------------------------- */
static void makedot(WINDOW *win, int li, int col, int ch)
{
if ('_'==ch) {
	mvwaddch(win, li, col,   ' ');
	mvwaddch(win, li, col+1, ' ');
	}
else	{
	wstandout(win);
	mvwaddch(win, li, col,   '+');
	mvwaddch(win, li, col+1, '+');
	wstandend(win);
	}
}
/* ---------------------------------------------------------------- */

int minidigit_0(WINDOW *win, int lig, int col, char digit, int k)
{
static char LX[]    = "_X_X_X_X_X_X_X_";
static char L0[]    = "XXXX_XX_XX_XXXX";
static char L1[]    = "_X__X__X__X__X_";
static char L2[]    = "XXX__XXXXX__XXX";
static char L3[]    = "XXX__XXXX__XXXX";
static char L4[]    = "X_XX_XXXX__X__X";
static char L5[]    = "XXXX__XXX__XXXX";
static char L6[]    = "XXXX__XXXX_XXXX";
static char L7[]    = "XXX__X__X__X__X";
static char L8[]    = "XXXX_XXXXX_XXXX";
static char L9[]    = "XXXX_XXXX__XXXX";

static char Lsp[]   = "_______________";		/* space */
static char Lmo[]   = "______XXX______";		/* moins */
static char Lco[]   = "____X_____X____";		/* colomn */
static char Ldp[]   = "_____________X_";		/* decimal dot */

char		*cptr;
int	l, c;

switch (digit) {
	
	case '0':	cptr = L0;		break;
	case '1':	cptr = L1;		break;
	case '2':	cptr = L2;		break;
	case '3':	cptr = L3;		break;
	case '4':	cptr = L4;		break;
	case '5':	cptr = L5;		break;
	case '6':	cptr = L6;		break;
	case '7':	cptr = L7;		break;
	case '8':	cptr = L8;		break;
	case '9':	cptr = L9;		break;

	case ' ':	cptr = Lsp;		break;
	case '-':	cptr = Lmo;		break;
	case ':':	cptr = Lco;		break;
	case '.':	cptr = Ldp;		break;

	default:	cptr = LX;		break;

	}

for (l=0; l<5; l++) {
	for (c=0; c<3; c++) {
		makedot(win, l+lig, (c*2)+col, *cptr++);
		}
	}

wrefresh(win);

return 0;
}
/* ---------------------------------------------------------------- */
int minidigit_affstr(WINDOW *win, int lig, int col, char *str)
{
int	foo, len;

len = strlen(str);

for (foo=0; foo<len; foo++) {
	minidigit_0(win, lig, col, str[foo], 0);
	col += 8;
	}

return 0;
}
/* ---------------------------------------------------------------- */
int minidigit_HMS(WINDOW *win, int lig, int col, int k)
{
int		foo;
char		chaine[20];
struct tm       *p_tms;
time_t		temps;

temps = time(NULL);
p_tms = localtime(&temps);
(void)strftime(chaine, 19, "%H:%M:%S", p_tms);
for (foo=0; foo<strlen(chaine); foo++) {
	minidigit_0(stdscr, lig, col+foo*8, chaine[foo], 0);
	}
return 0;
}
/* ---------------------------------------------------------------- */
