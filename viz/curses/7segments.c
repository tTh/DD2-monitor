/*
 *		DD2 Monitoring
 *
 *	ncurses seven segment display
 */

/**********************************************************************
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
**********************************************************************/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <ctype.h>
#include  <getopt.h>
#include  <ncurses.h>

#include  "ecran.h"

extern int	verbosity;

/* ---------------------------------------------------------------- */
int aff7segs_base(WINDOW * win, int lig, int col, int bits, int k)
{
int	numbit, mask;
int	foo;

for (foo=0; foo<9; foo++) {
	mvwhline(win, lig+foo, col, '~', 6);
	}

#define KC  '+'
#define KS  "++++"

wstandout(win);
for (numbit=0; numbit<8; numbit++) {
	mask = 1 << numbit;
	switch(mask & bits) {
		case 0x01:
			mvwaddstr(win, lig, col+1, KS);
			break;
		case 0x02:
			mvwaddch(win, lig+1, col+5, KC);
			mvwaddch(win, lig+2, col+5, KC);
			mvwaddch(win, lig+3, col+5, KC);
			break;
		case 0x04:
			mvwaddch(win, lig+5, col+5, KC);
			mvwaddch(win, lig+6, col+5, KC);
			mvwaddch(win, lig+7, col+5, KC);
			break;
		case 0x08:
			mvwaddstr(win, lig+8, col+1, KS);
			break;
		case 0x10:
			mvwaddch(win, lig+5, col, KC);
			mvwaddch(win, lig+6, col, KC);
			mvwaddch(win, lig+7, col, KC);
			break;
		case 0x20:
			mvwaddch(win, lig+1, col , KC);
			mvwaddch(win, lig+2, col , KC);
			mvwaddch(win, lig+3, col , KC);
			break;
		case 0x40:
			mvwaddstr(win, lig+4, col+1, KS);
			break;
		case 0x80:
			/* decimal point */
			mvwaddch(win, lig+7, col+2 , KC);
			mvwaddch(win, lig+7, col+3 , KC);
			mvwaddch(win, lig+8, col+2 , KC);
			mvwaddch(win, lig+8, col+3 , KC);
			break;

		}

	}
wstandend(win);

return 0;
}
/* ---------------------------------------------------------------- */
int aff7segs_digit(WINDOW * win, int lig, int col, char digit)
{
int		bits;

#if TRACE > 1
fprintf(stderr, ">>> %s ( %p %d %d '%c' )\n", __func__,
			win, lig, col, digit);
#endif

if (isxdigit(digit))   digit = toupper(digit);

switch (digit) {
	case '0':	bits = 0x3f;		break;
	case '1':	bits = 0x06;		break;
	case '2':	bits = 0x5b;		break;
	case '3':	bits = 0x4f;		break;
	case '4':	bits = 0x66;		break;
	case '5':	bits = 0x6d;		break;
	case '6':	bits = 0x7d;		break;
	case '7':	bits = 0x07;		break;
	case '8':	bits = 0x7f;		break;
	case '9':	bits = 0x6f;		break;

	/* hexadecimal letters */
	case 'A':	bits = 0x77;		break;
	case 'B':	bits = 0x7c;		break;
	case 'C':	bits = 0x39;		break;
	case 'D':	bits = 0x5e;		break;
	case 'E':	bits = 0x79;		break;
	case 'F':	bits = 0x71;		break;

	case ' ':	bits = 0;		break;
	case '.':	bits = 0x80;		break;
	case '-':	bits = 0x40;		break;

	default:	bits = 0x49;		break;
	}

aff7segs_base(win, lig, col, bits, 0);

return 0;
}
/* ---------------------------------------------------------------- */
int aff7segs_short(WINDOW * win, int lig, int col, short value)
{
char	buff[10];
int	idx;

sprintf(buff, "%6d", value);
// mvwaddstr(win, lig-1, col, buff);
for (idx=0; idx<strlen(buff); idx++) {
	aff7segs_digit(win, lig, col+(idx*9), buff[idx]);
	}
wrefresh(win);

return 0;
}
/* ---------------------------------------------------------------- */
int aff7segs_float(WINDOW * win, int lig, int col, float value)
{
char	buff[10];
int	idx;

sprintf(buff, "%6.2f", value);
// mvwaddstr(win, lig-1, col, buff);
for (idx=0; idx<strlen(buff); idx++) {
	aff7segs_digit(win, lig, col+(idx*9), buff[idx]);
	}
wrefresh(win);

return 0;
}

/* ---------------------------------------------------------------- */
