/**********************************************************************
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
**********************************************************************/
#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>
#include  <string.h>
#include  <time.h>
#include  <math.h>
#include  <getopt.h>
#include  <ncurses.h>

#include  "ecran.h"

int	verbosity;

/* ---------------------------------------------------------------- */
int demo_minidigits(int nbl, int k)
{
int		loop, foo;
char		chaine[100];

for (loop=0; loop<nbl; loop++) {

	sprintf(chaine, "== %06X ==", loop);
	aff_message(chaine);

	sprintf(chaine, ".%08d.", rand()%1000000);

	/***
	for (foo=0; foo<10; foo++) {
		minidigit_0(stdscr, 5, 2+foo*8, chaine[foo], 0);
		}
	***/
	minidigit_affstr(stdscr, 5, 2, chaine);
	wrefresh(stdscr);
	usleep(350*1000);

/***
	for (foo=0; foo<10; foo++) {
		minidigit_0(stdscr, 5, 2+foo*8, ' ', 0);
		}
***/

	minidigit_HMS(stdscr, 15, 9, 0);

	wrefresh(stdscr);
	usleep(250*1000);
	}
return -1;
}
/* ---------------------------------------------------------------- */
int demo_composite(int nbl, int k)
{
int		loop, foo;
short		sval;
char		ligne[120];
float		fval;

for (loop=0; loop<nbl; loop++) {
	sval = (short)((loop % 1024)-512);

	foo = aff7segs_short(stdscr, 5, 3, sval);
	if (foo) aff_message("KRKRK aff7 short");

	fval = (float)sval / 1024.0;
	foo = aff7segs_float(stdscr, 16, 3, fval);
	if (foo) aff_message("KRKRK aff7 float");

	fval = fabs(fval);
	foo = vumetre_0(stdscr, 29, 5, fval, COLS-10);

	sprintf(ligne, "%04x", loop);
	mvaddstr(2, 1, ligne);
	refresh();
	usleep(200*1000);
	}
return 0;
}
/* ---------------------------------------------------------------- */
void demo_7segments(int nbl, int notused)
{
int		loop, idx, foo;
char		ligne[120];
time_t		temps;
struct tm       *p_tms;

for (loop=0; loop<nbl; loop++) {
	sprintf(ligne, "%04x", loop);
	mvaddstr(2, 1, ligne);
	for (idx=0; idx<strlen(ligne); idx++) {
		aff7segs_digit(stdscr, 3, 10+(idx*9), ligne[idx]);
		}

	sprintf(ligne, "%.3f", drand48());
	mvaddstr(13, 1, ligne);
	for (idx=0; idx<strlen(ligne); idx++) {
		aff7segs_digit(stdscr, 14, 10+(idx*9), ligne[idx]);
		}

	if (verbosity && (loop%2)) {
		temps = time(NULL);
		p_tms = localtime(&temps);
		foo = strftime(ligne, 100, "%F %H:%M", p_tms);
		// sprintf(ligne, "%12ld | %s", temps, ctime(&temps));
		aff_message(ligne);
		}

	refresh();
	usleep(400*1000);
	}

}
/* ---------------------------------------------------------------- */
void demo_vumetres(int nbl, int notused)
{
int		loop, idx;
int		hpos;
char		ligne[100];
float		value;
time_t		temps;

for (loop=0; loop<nbl; loop++) {

	value = (float)loop / (float)nbl;

	for (idx=0; idx<8; idx++) {

		hpos = 4 * (idx+1);
		value = drand48();
		
		if (idx<4) vumetre_0(stdscr, hpos, 12, value, 60);
		else       vumetre_1(stdscr, hpos, 12, value, 60);

		}

	if (verbosity && (loop%2)) {
		temps = time(NULL);
		sprintf(ligne, "%12ld | %s", temps, ctime(&temps));
		aff_message(ligne);
		}
	
	refresh();
	usleep(200*1000);
	}

}
/* ---------------------------------------------------------------- */
void demo_waterfall(int nbl, int k)
{
int		loop, foo;
char		line[100];
WINDOW		*water;
static float	rvals[4];
struct timespec	ts;

ts.tv_sec = 0;
ts.tv_nsec = 200 * 1000 * 1000;

water = open_waterfall("premier essai", 0);

for (loop=0; loop<nbl; loop++) {

	sprintf(line, " %06X  %04X ", loop, rand()&0xffff);
	aff_message(line);

	wrefresh(stdscr);

	for (foo=0; foo<4; foo++) {
		if (rand()%100<42) {
			rvals[foo] += 4.04 * (foo + 1);
			}
		if (rvals[foo] > 1023.0) {
			rvals[foo] = (float)(rand() % 25);
		}
	}

	plot_waterfall(water, 1, rvals);

	/* if (rand()%10 < 1)	sleep(1); */
	foo = nanosleep(&ts, NULL);
	if (foo) {
		/* got a signal ? */
		aff_message("err on nanosleep");
		}
	}

close_waterfall(water, 0);

}
/* ---------------------------------------------------------------- */
static void finish(int signal)
{
endwin();       exit(0);
}
/* ---------------------------------------------------------------- */
int main (int argc, char *argv[])
{
int		opt;
int		demonum = 0;
int		nb_loops = 200;

/* set some default values */
verbosity	= 0;

while ((opt = getopt(argc, argv, "n:vy:")) != -1) {
	switch (opt) {
		case 'n':	nb_loops = atoi(optarg);	break;

		case 'v':	verbosity++;			break;

		case 'y':	demonum = atoi(optarg);		break;		

		default:
			fprintf(stderr, "%s : uh ?", argv[0]);
			exit(1);
			break;
		}

	}

initscr();
nonl();         cbreak();       noecho();

keypad(stdscr, TRUE);           /* acces aux touches 'curseur' */

fond_ecran(" Ncurses Eyecandy ");

switch (demonum) {
	case 0:		demo_vumetres(nb_loops, 0);		break;
	case 1:		demo_waterfall(nb_loops, 0);		break;
	case 2:		demo_7segments(nb_loops, 0);		break;
	case 3:		demo_composite(nb_loops, 0);		break;
	case 4:		demo_minidigits(nb_loops, 0);		break;

	default:
		fprintf(stderr, "eyecandy #%d don't exist\n", demonum);
		break;
	}

/*
 *      plop, on a fini, restaurer la console
 */
finish(0);

return 0;
}
/* ---------------------------------------------------------------- */
