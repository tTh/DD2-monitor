/*
 *		DD2 Monitoring
 *
 *	ncurses waterfall interface
 */


#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <getopt.h>
#include  <ncurses.h>


#include  "ecran.h"

extern int	verbosity;

/* ---------------------------------------------------------------- */
WINDOW *open_waterfall(char *title, int flags)
{
WINDOW		*win;
int		l, c, w, h;

l = 1;			c = 1;
w = COLS;		h = LINES - 3;

win = newwin(h, w, l, c);
scrollok(win, 1);
waddstr(win, title);	waddch(win, '\n');
wrefresh(win);

return win;
}
/* ---------------------------------------------------------------- */
int plot_waterfall(WINDOW *wf, int mode, float values[4])
{
#define TL 1000
int		foo, idx;
char		tag, ligne[TL+1];
float		coef_w;
static long	iter;

if (0 == (iter%10)) {
	memset(ligne, '.', TL);
	}
else	{
	memset(ligne, ' ', TL);
	}
for (foo=0; foo<500; foo+=20) {
	ligne[foo] = '.';
	}
ligne[COLS-1] = '\0';

iter++;

coef_w = (float)(COLS-1) / 1024.0;

#if TRACE > 1
fprintf(stderr, "COLS = %d, coef_w = %f\n", COLS, coef_w);
#endif

switch (mode) {

	case 0:	default:
		sprintf(ligne, "%11.3f  %11.3f  %11.3f  %11.3f",
			values[0], values[1], values[2], values[3]);
#if TRACE
		fprintf(stderr, "%s [%s]\n", __func__, ligne);
#endif
		break;

	case 1:
		for(foo=0; foo<4; foo++) {
			tag = "ATOX"[foo];
			idx = (int)(values[foo]*coef_w);
			ligne[idx] = tag;
#if TRACE
			fprintf(stderr, "%c   %3d  ", tag, idx);
#endif
			}
		ligne[COLS-1] = '\0';
#if TRACE
			fprintf(stderr, "\n"); fflush(stderr);
#endif

		break;

	}



/* poke the text in the curses window */
// scroll(wf);
waddstr(wf, ligne);	waddch(wf, '\n');
wrefresh(wf);

return -1;
}
/* ---------------------------------------------------------------- */
int close_waterfall(WINDOW *wf, int notused)
{
int foo;

if (NULL == wf) {
	fprintf(stderr, "%s wf is null\n", __func__);
	return -1;
	}

foo = delwin(wf);
if (ERR==foo) {
	fprintf(stderr, "%s : err on delwin\n", __func__);
	return -1;
	}
return 0;
}
/* ---------------------------------------------------------------- */
