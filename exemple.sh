#!/bin/bash

DATAFILE=/tmp/fake-datafile

#-----  collect the datas
> ${DATAFILE}
for s in $(seq 1 1000)
do
    v=$(./fake-values -s -t 4)
    echo $s $v >> ${DATAFILE}
done

tail -5 ${DATAFILE}

#-----  do dome useless computations
awk	'
	NR==1	{
		debut = $2
		}
		
		{
		# print $2-debut, $3
		v = int($3/35);
		bucket[v]++;
		}
		
	END	{
		for (v=0; v<40; v++) {
			printf "%4d ", v;
			for (foo=0; foo<bucket[v]/2; foo++) {
				printf "-";
				}
			print "*"
			}
		}
	'                                   \
< ${DATAFILE}



